#include "main.h"

int main()
{
    // Vytvor promennou oznacujici pocatecni uzel, koncovy uzel
    // Vytvor vektor vektoru pro vstupni matici predstavujici graf
    int start;
    int end;
    std::vector<std::vector<int>> adjMat;

    // Nacti matici
    getMatrix(adjMat);

    // Pokud je matice v poradku, nacti start a cil a najdi mezi temito
    // uzly grafu nejkratsi cestu pomoci Dijkstrova algoritmu
    if (checkMatrix(adjMat)) {
        std::cout << "Zadejte pocatecni uzel:";
        std::cin >> start;
        std::cout << "Zadejte koncovy uzel:";
        std::cin >> end;
        dijkstra(adjMat, start, end);
    }

    return 0;
}