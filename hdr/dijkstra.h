#include <vector>
#include <climits>
#include <cstdio>
#include <iostream>


int minDistance(int V, int dist[], bool doneSet[]);
void printPath(int parent[], int j);
void printSolution(int V, int src, int dist[], int parent[]);
void dijkstra(std::vector<std::vector<int>> &vec, int src, int end);
void printSolutionToEnd(int src, int dist[], int parent[], int end);