#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

void getMatrix(std::vector<std::vector<int>> &adjMat);
bool checkMatrix(std::vector<std::vector<int>> &adjMat);
void printMatrix(std::vector<std::vector<int>> &vec);
