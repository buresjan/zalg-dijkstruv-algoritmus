#include "../hdr/dijkstra.h"

// Pomocna funkce k nalezeni indexu uzlu neobsazenem v poli probranych
// uzlu, ktery ma nejkratsi vzdalenost
int minDistance(int V, int dist[], bool doneSet[])
{
    int min = INT_MAX, min_index;   // Inicializace nejkratsi vzdalenosti

    for (int v = 0; v < V; v++)                // Prober vsechny uzly ne v poli
        if (!doneSet[v] && dist[v] <= min)     // probranych uzlu a updatuj
            min = dist[v], min_index = v;      // min vzdalenost a index nejblizsiho

    return min_index;
}

// Funkce pro rekurzivni vypsani cesta do vrcholu j od startu
void printPath(int parent[], int j)
{
    if (parent[j] == - 1)   // Pokud hledas predchozi uzel od startu,
        return;             // tak ukonci rekurzi

    printPath(parent, parent[j]);

    std::cout << " " << j;
}

// Funkce pro vypsani cest do vsech vrcholu od startu
// Program ma primarne vypsat cestu start-cil, proto neni fce pouzita
void printSolution(int V, int src, int dist[], int parent[])
{
    std::cout << std::endl;
    std::cout << "Kam\t\t Vzdalenost\t\tCesta";
    for (int i = 0; i < V; i++)
    {
        if (dist[i] == INT_MAX) {
            std::cout << "\n" << src << "->" << i << "\t\t ";
            std::cout <<"-\t\tVrcholu nelze dosahnout";
        }
        else {
            std::cout << "\n" << src << "->" << i << "\t\t " << dist[i] << "\t\t " << src;
            printPath(parent, i);
        }
    }
}

// Funkce pro vypsani vzdalenost a nejkratsi cesty start-cil
void printSolutionToEnd(int src, int dist[], int parent[], int end)
{
    std::cout << "Kam\t\tVzdalenost\t  Cesta";
    if (dist[end] == INT_MAX) {                             // Vypis chybovou hlasku
        std::cout << "\n" << src << "->" << end << "\t\t "; // v pripade, ze vrcholu
        std::cout << "-\t\tVrcholu nelze dosahnout";        // nelze dosahnout
    }
    else {
        std::cout << "\n" << src << "->" << end << "\t\t " << dist[end] << "\t\t" << src;
        printPath(parent, end);
    }
}

// Funkce implementujici samotny Dijkstruv algoritmus vyuzivajici adjacencni
// matice
void dijkstra(std::vector<std::vector<int>> &adjMat, int start, int end)
{
    int V = adjMat.size();  // Uloz pocet uzlu v grafu
    int dist[V]; // Pole pro ulozeni nejkratsi vzdalenosti do i-teho vrcholu
                 // od startovniho uzlu

    bool doneSet[V]; // Prvek doneSet[i] je true v pripade, kdy jiz byla do i-teho
                     // uzlu nalezena nejkratsi cesta

    int parent[V];  // Pole pro ulozeni cesty do cile

    if (start >= V || end >= V || start < 0 || end < 0) {     // Zkontroluj zadane meze
        std::cout << "\nSpatne zadany pocatecni nebo koncovy uzel...";
        return;
    }

    // Inicializuj vsechny minimalni vzdalenosti jako INT_MAX a nastav pro vsechny
    // uzly stav, ze do nich nebyla nalezena nejkratsi cesta
    for (int i = 0; i < V; i++) {
        dist[i] = INT_MAX;
        doneSet[i] = false;
    }

    // Nastav zarazku pro rekurzivni hledani cesty
    parent[start] = -1;

    // Vzdalenost pocatecniho vrcholu od sebe samotneho je vzdy 0
    dist[start] = 0;

    // Hledej nejkratsi cestu pro vsechny vrcholy
    for (int j = 0; j < V - 1; j++) {
        // Vyber uzel z mnoziny uzlu, ktere jeste nebyly zpracovany
        // s nejkratsi vzdalenosti
        // V prvni iteraci je u = start
        int u = minDistance(V, dist, doneSet);

        // Pokud jsi nasel nejkratsi cestu do zadaneho koncoveho uzlu, vypis
        // vzdalenost a cestu od startu a ukonci cyklus hledani
        /*if (u == end) {
            printSolutionToEnd(start, dist, parent, end);
            return;
        }*/

        // Pridej nalezeny uzel s nejkratsi vzdalenosti do mnoziny zpracovanych uzlu
        doneSet[u] = true;

        // Prirad nove hodnoty nejkratsi vzdalenosti sousednich vrcholu od vybraneho vrcholu
        for (int v = 0; v < V; v++)
            // Aktualizuj vzdalenost do uzlu v jen pokud neni v mnozine jiz zpracovanych
            // vrcholu, existuje hrana z u do v a cesta do u plus hrana do v vylepsi
            // soucasnou nejkratsi vzdalenost do v
            if (!doneSet[v] && adjMat[u][v] && dist[u] + adjMat[u][v] < dist[v]
                && dist[u] != INT_MAX) {
                parent[v] = u;
                dist[v] = dist[u] + adjMat[u][v];
            }
    }
    if (!doneSet[end]) {  // Na konci zkotroluj, zda byla nalezena nejkratsi cesta do cile
        std::cout << "Kam\t\t Vzdalenost\t\tCesta";
        std::cout << "\n" << start << "->" << end << "\t\t ";
        std::cout << "-\t\tVrcholu nelze dosahnout";
    }

    // Pripraveno pro odkomenotvani a upraveni konce cyklu v pripade, kdy chceme, aby program
    // hledal nejkratsi cesty do vsech uzlu

    printSolution(V, start, dist, parent);
    std::cout << std::endl;

}