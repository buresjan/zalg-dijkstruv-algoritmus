#include "../hdr/graph.h"

// Funkce pro nacteni adjacencni matice
void getMatrix(std::vector<std::vector<int>> &adjMat)
{
    //std::ifstream file(R"(C:\Users\bures\CLionProjects\dijkstra1.1\sample_input.txt)");
    std::ifstream file("sample_input.txt");
    if (!file) {std::cout << "Soubor se nepodarilo otevrit" << std::endl;}
    std::string line;
    while (std::getline(file, line)) // Precti nasledujici radek a uloz ho do 'line'
                                            // skonci, kdyz uz nebude zadny dalsi radek
    {
        // Z line 'line' udelej 'string stream'
        std::istringstream ss(line);

        adjMat.push_back({}); // Pridej do 'adjMat' prazdny vektor

        int x;
        while (ss >> x) // Cti ze stringstreamu dalsi int do x
                        // Skonci, kdyz uz nebude zadny dalsi int
            adjMat.back().push_back(x); // Pridej x do posledniho podvektoru 'adjMat'
    }

    printMatrix(adjMat); // Vypis adjacencni matici
}

// Funkce pro kontrolu vstupu ze souboru
bool checkMatrix(std::vector<std::vector<int>> &adjMat)
{
    bool flag = true;
    // V souboru musi byt pouze nezaporna cisla, pro zaporna Dijkstra nefunguje
    for (int i = 0; i < adjMat.size(); i++) {
        for (int j = 0; j < adjMat[i].size(); j++)
            if(adjMat[i][j] < 0) {flag = false;}
    }
    int i = 0;
    // Vsechny radky musi byt stejne dlouhe
    while (flag && i < adjMat.size()){
        if (adjMat[i].size() != adjMat.size()) {flag = false;}
        else {}
        i++;
    }
    // Vstupni soubor nesmi byt prazdny
    if (adjMat.empty()) {flag = false;}
    return flag;
}

// Funkce pro vypsani matice ci chybove hlasky
void printMatrix(std::vector<std::vector<int>> &vec)
{
    bool check = checkMatrix(vec);
    if (check) {
        std::cout << "Adjacencni matice zadaneho grafu:" << std::endl;
        std::cout << "#################################" << std::endl;

        for (int i = 0; i < vec.size(); i++) {
            for (int j = 0; j < vec[i].size(); j++)
                std::cout << vec[i][j] << " ";
            std::cout << std::endl;
        }

        std::cout << "#################################" << std::endl;
    }
    else {
        std::cout << "Zadana adjacencni matice neni v poradku (spatny rozmer/zaporna cisla)...";
    }

}